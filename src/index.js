import "./style.css";
let forms_NodeList = document.querySelectorAll(".myForm");
let forms = Array.from(forms_NodeList);
let buttons = document.querySelectorAll(".addButton");
let uls_NodeList = document.querySelectorAll('.toDoList');
let uls = Array.from(uls_NodeList); // an array of our lists in each cell
let inputs_NodeList = document.querySelectorAll('.textInput');
let inputs = Array.from(inputs_NodeList); //an array of our inputs to get a value


buttons.forEach( function (item) {
    item.addEventListener('click', function() {
        let button_number = item.dataset.number; //получаем номер нашего окошка с днем
        let ul = uls[button_number-1]; //номер нащшего списка дел соответствует номеру нашей таблицы

        let li = document.createElement('li');
        li.classList.add("listItem");
        li.innerHTML = `
            <label class="container">${inputs[button_number-1].value}
                <input type="checkbox" class="DoneCheckbox">
                <span class="checkmark"></span>
            </label>      
        `;

        ul.appendChild(li);

        let resetForm = () => {
            forms[button_number-1].reset();
        };

        resetForm();
    })
})

//as for now only for one tip

let tip_buttons = document.querySelectorAll(".tip_button");
let close_buttons = document.querySelectorAll(".close_button");
let tip1 = document.getElementById('tip1');

console.log(close_buttons);
console.log(tip1);

close_buttons.forEach( function (item) {
    item.addEventListener('click', function() {
        tip1.classList.add("hide")
    })
});

tip_buttons.forEach( function (item) {
    item.addEventListener('click', function(){
        tip1.classList.remove('hide');
    })
})
