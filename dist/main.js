/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _style_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.css */ \"./src/style.css\");\n/* harmony import */ var _style_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_css__WEBPACK_IMPORTED_MODULE_0__);\n\r\nlet forms_NodeList = document.querySelectorAll(\".myForm\");\r\nlet forms = Array.from(forms_NodeList);\r\nlet buttons = document.querySelectorAll(\".addButton\");\r\nlet uls_NodeList = document.querySelectorAll('.toDoList');\r\nlet uls = Array.from(uls_NodeList); // an array of our lists in each cell\r\nlet inputs_NodeList = document.querySelectorAll('.textInput');\r\nlet inputs = Array.from(inputs_NodeList); //an array of our inputs to get a value\r\n\r\n\r\nbuttons.forEach( function (item) {\r\n    item.addEventListener('click', function() {\r\n        let button_number = item.dataset.number; //получаем номер нашего окошка с днем\r\n        let ul = uls[button_number-1]; //номер нащшего списка дел соответствует номеру нашей таблицы\r\n\r\n        let li = document.createElement('li');\r\n        li.classList.add(\"listItem\");\r\n        li.innerHTML = `\r\n            <label class=\"container\">${inputs[button_number-1].value}\r\n                <input type=\"checkbox\" class=\"DoneCheckbox\">\r\n                <span class=\"checkmark\"></span>\r\n            </label>      \r\n        `;\r\n\r\n        ul.appendChild(li);\r\n\r\n        let resetForm = () => {\r\n            forms[button_number-1].reset();\r\n        };\r\n\r\n        resetForm();\r\n    })\r\n})\r\n\r\n//as for now only for one tip\r\n\r\nlet tip_buttons = document.querySelectorAll(\".tip_button\");\r\nlet close_buttons = document.querySelectorAll(\".close_button\");\r\nlet tip1 = document.getElementById('tip1');\r\n\r\nconsole.log(close_buttons);\r\nconsole.log(tip1);\r\n\r\nclose_buttons.forEach( function (item) {\r\n    item.addEventListener('click', function() {\r\n        tip1.classList.add(\"hide\")\r\n    })\r\n});\r\n\r\ntip_buttons.forEach( function (item) {\r\n    item.addEventListener('click', function(){\r\n        tip1.classList.remove('hide');\r\n    })\r\n})\r\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/style.css":
/*!***********************!*\
  !*** ./src/style.css ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// removed by extract-text-webpack-plugin\n\n//# sourceURL=webpack:///./src/style.css?");

/***/ })

/******/ });